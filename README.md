# Java Socket Chat

This is a simple Java chat system built for ITCS 3166 (Intro to Computer Networks) that has a GUI client and a headless server.

Features include:

* Apache Ant build file for easy deployment
* Command system for users to join/leave channels, change their name, test their connection, and send private messages
* GUI for the client
* Gracefully handles unexpected client & server disconnects
* Handles up to 10 clients by default, but can be increased to your hearts desire
* Letting users specify the hostname, port, and a username
* Lists active users in the channel
* Pre-compiled JAR files (in the `dist` folder) for easy running
* User-creatable channels (chat rooms)
* Whispers (private messages)

### Starting the Server

To start the server, simply run this command:

```bash
java -jar dist/Server.jar
```

It should be run from command prompt, PowerShell, bash, etc. since it is a headless server - it does not have a GUI. You can run it by double clicking on it, but you will have to close it via `kill` or task manager.

Alternatively, if you want to build from source, an Apache Ant build file is included to simplify deployment. After you have installed Ant (the `ant` package for Debian-based Linux distros, you can find instructions for installing Ant on Windows [here](https://ant.apache.org/manual/install.html)), run these commands:

```bash
ant build
ant Server
```

Alternatively, you can import this git repo as an Eclipse project and run `Server.java`.

### Starting the Client

To start the client, you can run `dist/Client.jar` by double clicking on it or by running this command:

```bash
java -jar dist/Client.jar
```

Assuming you have Apache Ant installed on your system, building from source and starting the client is easy:

```bash
ant build
ant Client
```

Alternatively, you can import this git repo as an Eclipse project and run `Client.java`.

To increase the number of clients that can connect to the server, update `maxClients` in `Server.java`.

### Credit

This was a group project. Luna Lucadou and Elijah Leatherwood were primary contributors, with Shiv Parekh and Matthew Bavis helping to test the application.
