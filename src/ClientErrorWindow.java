import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class ClientErrorWindow {

	private JFrame frmSocketChat;
	private String errorPrimary;
	private String errorSecondary;
	private boolean closeApplication = false;

	/**
	 * Create the application.
	 */
	public ClientErrorWindow(String primary, String secondary, boolean closeOnOk) {
		if (primary != null) {
			this.errorPrimary = primary;
		} else {
			this.errorPrimary = "[Sample Text]";
		}
		if (secondary != null) {
			this.errorSecondary = secondary;
		} else {
			this.errorSecondary = "[Sample Text 2]";
		}
		initialize();
		
		this.closeApplication = closeOnOk;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSocketChat = new JFrame();
		frmSocketChat.setTitle("3166 Socket Chat Error");
		frmSocketChat.setBounds(100, 100, 450, 250);
		if (closeApplication) {
			frmSocketChat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		} else {
			frmSocketChat.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		}
		frmSocketChat.getContentPane().setLayout(new MigLayout("", "[100%,grow]", "[][][grow][][][][]"));
		
		JLabel lblError = new JLabel("Error");
		frmSocketChat.getContentPane().add(lblError, "cell 0 0,alignx center");
		
		JLabel lblPrimaryError = new JLabel(errorPrimary);
		frmSocketChat.getContentPane().add(lblPrimaryError, "cell 0 1");
		
		JScrollPane scrollPane = new JScrollPane();
		frmSocketChat.getContentPane().add(scrollPane, "cell 0 2 1 3,grow");
		
		JTextPane txtpnSecondaryError = new JTextPane();
		txtpnSecondaryError.setEditable(false);
		txtpnSecondaryError.setText(errorSecondary);
		scrollPane.setViewportView(txtpnSecondaryError);
		
		JButton btnOk = new JButton("Ok");
		btnOk.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frmSocketChat.dispose();
				if (closeApplication) {
					System.exit(0);
				}
			}
		});
		
		Component verticalStrut = Box.createVerticalStrut(20);
		frmSocketChat.getContentPane().add(verticalStrut, "cell 0 5");
		frmSocketChat.getContentPane().add(btnOk, "cell 0 6,alignx center");
		
		frmSocketChat.setVisible(true);
	}
}
