import javax.swing.JFrame;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Component;
import javax.swing.Box;

public class ClientConnectWindow {

	private JFrame frmSocketChat;
	private JTextField txtServerAddress;
	private JTextField txtPortNumber;
	private JTextField txtUsername;
	private JButton btnConnect;
	private JButton btnCancel;
	private String server;
	private String port;
	private String username;
	private boolean windowClosed;
	private Component verticalStrut;

	/**
	 * Creates the connection options window
	 */
	public ClientConnectWindow() {
		this.server = "127.0.0.1";
		this.port = "5000";
		this.username = "";
		this.initialize();
	}
	
	/**
	 * Creates the connection options window
	 * @param server the server to connect to
	 * @param port the port to connect on
	 * @param username the username to assume after connecting
	 */
	public ClientConnectWindow(String server, String port, String username) {
		if (server != null) {
			this.server = server;
		} else {
			this.server = "127.0.0.1";
		}
		if (port != null) {
			this.port = port;
		} else {
			this.port = "5000";
		}
		if (username != null) {
			this.username = username;
		} else {
			this.username = "";
		}
		this.initialize();
	}
	
	/**
	 * Creates the connection options window
	 * @param server the server to connect to
	 * @param port the port to connect on
	 * @param username the username to assume after connecting
	 * @param applicationStartup if the application is going through initial startup or if the GUI client is already running
	 */
	public ClientConnectWindow(String server, String port, String username, boolean applicationStartup) {
		if (server != null) {
			this.server = server;
		} else {
			this.server = "127.0.0.1";
		}
		if (port != null) {
			this.port = port;
		} else {
			this.port = "5000";
		}
		if (username != null) {
			this.username = username;
		} else {
			this.username = "";
		}
		
		this.initialize();
		frmSocketChat.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		windowClosed = false;
		frmSocketChat = new JFrame();
		frmSocketChat.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				windowClosed = true;
			}
		});
		frmSocketChat.setTitle("3166 Socket Chat Connect");
		frmSocketChat.setBounds(100, 100, 450, 300);
		frmSocketChat.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmSocketChat.getContentPane().setLayout(new MigLayout("", "[grow][grow][grow]", "[][][][][][][]"));
		
		JLabel lblConnect = new JLabel("Connect to Chat Server");
		frmSocketChat.getContentPane().add(lblConnect, "cell 0 0 3 1,alignx center");
		
		JLabel lblServerAddress = new JLabel("Server Address");
		frmSocketChat.getContentPane().add(lblServerAddress, "cell 0 2");
		
		txtServerAddress = new JTextField();
		txtServerAddress.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					connect();
				}
			}
		});
		txtServerAddress.setToolTipText("IP address or hostname");
		txtServerAddress.setText(server);
		frmSocketChat.getContentPane().add(txtServerAddress, "cell 1 2 2 1,growx");
		txtServerAddress.setColumns(10);
		
		JLabel lblPort = new JLabel("Port");
		frmSocketChat.getContentPane().add(lblPort, "cell 0 3");
		
		txtPortNumber = new JTextField();
		txtPortNumber.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					connect();
				}
			}
		});
		txtPortNumber.setToolTipText("Must be a number from 1 to 65,535");
		txtPortNumber.setText(port);
		frmSocketChat.getContentPane().add(txtPortNumber, "cell 1 3 2 1,growx");
		txtPortNumber.setColumns(10);
		
		JLabel lblUsername = new JLabel("Username");
		frmSocketChat.getContentPane().add(lblUsername, "cell 0 4");
		
		txtUsername = new JTextField();
		txtUsername.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					connect();
				}
			}
		});
		txtUsername.setText(username);
		txtUsername.setToolTipText("Must be alphanumeric characters, no spaces");
		frmSocketChat.getContentPane().add(txtUsername, "cell 1 4 2 1,growx");
		txtUsername.setColumns(10);
		
		btnConnect = new JButton("Connect");
		btnConnect.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					connect();
				}
			}
		});
		btnConnect.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				connect();
			}
		});
		
		verticalStrut = Box.createVerticalStrut(20);
		frmSocketChat.getContentPane().add(verticalStrut, "cell 1 5");
		frmSocketChat.getContentPane().add(btnConnect, "cell 0 6,alignx right");
		
		btnCancel = new JButton("Cancel");
		btnCancel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				windowClosed = true;
				frmSocketChat.dispose();
			}
		});
		frmSocketChat.getContentPane().add(btnCancel, "flowx,cell 1 6,alignx right");
	}
	
	/**
	 * Evaluates if the server, port, and username are valid, and if so, starts the connection process
	 */
	private void connect() {
		// Check arguments
		boolean validServer = Client.isAddressValid(this.getAddress());
		boolean validPort = Client.isPortValid(this.getPort());
		boolean validUsername = Client.isUsernameValid(this.getUsername());
		boolean validParams = validServer && validPort && validUsername;
		
		if (validParams) {
			frmSocketChat.dispose(); // Hide Connect window
			windowClosed = true;
			Client.changeServerDetails(this.getAddress(), Integer.parseInt(this.getPort()), this.getUsername());
			Client.connect();
		} else {
			StringBuilder errors = new StringBuilder();
			if (!validServer)
				errors.append("Server address is invalid (does not appear to be an IPv4 address, IPv6 address, hostname, or FQDN)\n");
			if (!validPort)
				errors.append("Port is invalid (must be a whole number between 1-65,535)\n");
			if (!validUsername)
				errors.append("Username is invalid (must be alphanumeric characters, no spaces)\n");
			new ClientErrorWindow("Cannot connect due to the following errors:", errors.toString(), false);
		}
	}
	
	/**
	 * Gets the entered IP or hostname
	 * @return the address the user provided
	 */
	public String getAddress() {
		return txtServerAddress.getText();
	}
	
	/**
	 * Gets the entered port number
	 * @return the port number the user provided
	 */
	public String getPort() {
		return txtPortNumber.getText();
	}
	
	/**
	 * Gets the entered username (does not check to make sure it matches the required format)
	 * @return the username the user provided
	 */
	public String getUsername() {
		return txtUsername.getText();
	}
	
	/**
	 * Gets the status of the prompt for input
	 * @return
	 */
	public boolean isCompleted() {
		return windowClosed;
	}
}
