import java.util.ArrayList;

public class ClientChannel {
	private static final int RESIZE_TARGET = 200;
	private static final int RESIZE_THRESHOLD = 250;
	private ArrayList<String> messageList;
	private String name;
	private boolean hasUnread;
	
	/**
	 * Creates a new channel with a given name
	 * @param name the channel's name
	 */
	public ClientChannel(String name) {
		this.messageList = new ArrayList<String>();
		this.name = name;
		this.hasUnread = false;
	}
	
	/**
	 * Creates a channel with a given name and a set of messages the channel
	 * already has (intended for retaining startup messages in the main channel)
	 * @param name the channel's name
	 * @param initialMessages the set of messages to store with the channel
	 */
	public ClientChannel(String name, ArrayList<String> initialMessages) {
		this.messageList = new ArrayList<String>(initialMessages);
		this.name = name;
		this.hasUnread = false;
	}
	
	/**
	 * Adds a message to the channel's message log. If the channel is not the active channel, the unread
	 * messages log indicator will be toggled (if it is not already)
	 * @param message the message to store
	 * @return true if the message was added, false if the item failed to add due to an internal error
	 */
	public boolean addMessage(String message) {
		if (!Client.getActiveChannelName().equals(name)) {
			this.hasUnread = true;
		}
		// Determine if pruning is needed
		if (messageList.size() > ClientChannel.RESIZE_THRESHOLD) { // Default: 250
			// Resize messageList to be only the last RESIZE_TARGET messages (default: 200)
			System.out.println("Pruning messages in channel " + this.getName());
			messageList = new ArrayList<String>(messageList.subList(messageList.size() - ClientChannel.RESIZE_TARGET, messageList.size()));
			/* The reason I wait until 250 to resize to messages [50, 250) is because if I resized from messages [1, 201)
			 * every time the size hit 201, that would be a lot of array copying and deleting. This reduces how often
			 * the array clone & deletion occurs, which should make it far less taxing on the system.
			 * I prune the message list because if I stored an unlimited number of messages, that could use an excessive
			 * amount of memory over time.
			 * If the user has a channel open, they will not see messages disappear, it will only be visible to them
			 * when they navigate away and back to that channel.
			 * I could create a circular array to ensure only 200 messages are ever present, but this is much simpler
			 * and creating a circular array class would be overkill.
			 */
		}
		return messageList.add(message);
	}
	
	/**
	 * Adds a message to a channel's log with the option to not toggle the unread messages indicator
	 * @param message the message to store
	 * @param display if the client should display an unread message indicator
	 * @return true if the message was added, false if the item failed to add due to an internal error
	 */
	public boolean addMessage(String message, boolean display) {
		if (display && !Client.getActiveChannelName().equals(name)) {
			this.hasUnread = true;
		}
		
		return messageList.add(message);
	}
	
	/**
	 * Returns if there are any unread messages in the channel
	 * @return true if there are unread messages, false if there are none
	 */
	public boolean hasUnreads() {
		return this.hasUnread;
	}
	
	/**
	 * Gets the name of the channel
	 * @return the channel's name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Gets the messages in the channel
	 * @return the list of messages the channel has stored
	 */
	public ArrayList<String> getMessages() {
		return messageList;
	}
	
	/**
	 * Resets the unread messages status of the channel to false
	 */
	public void clearUnreads() {
		this.hasUnread = false;
	}
}
