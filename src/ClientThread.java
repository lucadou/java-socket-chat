import java.net.*;
import java.io.*;

public class ClientThread implements Runnable {
	private Socket socket;
	private Client client;
	private DataInputStream in;
	private boolean connected;
	
	/**
	 * Creates a thread to receive messages
	 * @param socket the socket to receive messages from
	 * @param client the spawning client
	 * @throws IOException
	 */
	public ClientThread(Socket socket, Client client) throws IOException {
		this.socket = socket;
		this.client = client;
		connected = true;
		in = new DataInputStream(socket.getInputStream());
	}

	/**
	 * Starts the message listener
	 */
	@Override
	public void run() {
		System.out.println("Listener thread opened");
		boolean b = true;
		while(b) {
			try {
				client.addMessage(in.readUTF());
			} catch (IOException e) {
				System.err.println("Error in ClientThread: " + e.getMessage());
				System.out.println("Assuming client disconnect");
				client.addMessage("Disconnected: Lost connection to server\nTo reconnect to the server, go to File -> Reconnect");
				connected = false;
				break;
			}
		}
		connected = false;
		System.out.println("Connection thread closed");
	}
	
	/**
	 * Returns if the client is still connected to the server
	 * @return the connection status of the client
	 */
	public boolean isConnected() {
		return connected && !socket.isClosed();
	}
	
	/**
	 * Disconnects the chat client from the server
	 * @throws IOException
	 */
	public void disconnect() throws IOException {
		socket.close();
		client.updateUsers();
	}
}
