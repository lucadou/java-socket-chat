import java.util.ArrayList;

public class ServerChannel {
	private ArrayList<String> connectedUsers;
	private boolean alwaysOn;
	private String name;
	
	/**
	 * Creates a channel in the system user's name; intended to be used for default channels
	 * @param name the name of the channel
	 * @param alwaysOn if the channel can be removed due to no users
	 */
	public ServerChannel(String name, boolean alwaysOn) {
		this.name = name;
		this.connectedUsers = new ArrayList<String>();
		this.addUser(Server.SYSTEM_NAME);
		this.alwaysOn = alwaysOn;
	}
	
	/**
	 * Creates a channel in a given user's name
	 * @param name the name of the channel
	 * @param creator the creator of the channel
	 * @param alwaysOn if the channel can be removed due to no users
	 */
	public ServerChannel(String name, String creator, boolean alwaysOn) {
		this.name = name;
		this.connectedUsers = new ArrayList<String>();
		this.addUser(Server.SYSTEM_NAME);
		this.addUser(creator);
		this.alwaysOn = alwaysOn;
	}
	
	/**
	 * Adds a user to the channel
	 * @param name the user to add to the channel
	 * @return true if the user was added, false if the user is already present or an internal error occurred
	 */
	public boolean addUser(String name) {
		if (connectedUsers.contains(name)) { // Prevents duplicates
			return false;
		}
		return connectedUsers.add(name);
	}
	
	/**
	 * Renames a user in the channel
	 * @param oldName the user's old name
	 * @param newName the user's new name
	 * @return true if the user was renamed successfully, false if the user was not renamed successfully or is not in the channel
	 */
	public boolean replaceUser(String oldName, String newName) {
		return connectedUsers.remove(oldName) && connectedUsers.add(newName);
	}
	
	/**
	 * Removes a user from the channel
	 * @param name the user to remove
	 * @return true if the user was removed, false if they failed to remove due to an internal error are not in the channel
	 */
	public boolean removeUser(String name) {
		return connectedUsers.remove(name);
	}
	
	/**
	 * Checks if a user is present in the channel
	 * @param user the user to search for
	 * @return true if the user is present in the channel, false if they are not in the channel
	 */
	public boolean hasUser(String user) {
		return connectedUsers.contains(user);
	}
	
	/**
	 * Gets the name of the channel
	 * @return the channel's name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Gets all the users connected to a channel
	 * @return the list of connected users
	 */
	public ArrayList<String> getUsers() {
		return this.connectedUsers;
	}
	
	/**
	 * Checks if the channel can be removed, intended for pruning unused channels
	 * @return true if the channel can be deleted, false if the channel is always on or has 1 or more connected users
	 */
	public boolean canClose() {
		// alwaysOn - cannot be closed
		// connectedUsers.size() > 1 - accounts for System user (always present)
		//System.out.println("[Debug] alwaysOn: " + alwaysOn + " connectedUsers.size: " + connectedUsers.size());
		return !alwaysOn && connectedUsers.size() <= 1;
	}
}
