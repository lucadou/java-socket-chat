import java.net.*;
import java.util.Arrays;
import java.io.*;

public class ServerThread extends Thread {
	private Socket socket;
	private int id;
	private DataInputStream in;
	private DataOutputStream out;
	private String nickname;
	
	/**
	 * Creates a new ServerThread object
	 * @param socket the network communications socket
	 * @param id the thread ID to include in the default name
	 * @throws IOException
	 */
	public ServerThread(Socket socket, int id) throws IOException {
		this.socket = socket;
		this.id = id;
		this.nickname = "User_" + id;
		in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		out = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
	}
	
	/**
	 * Returns if the user has a connection open and the thread is active.
	 * @return true if the connection is active
	 */
	public boolean isActive() {
		return !socket.isClosed();
		/* isConnected() - returns true if the socket ever successfully connected, regardless of current status.
		 * isBound() - returns true if the socket ever successfully bound, regardless of current status.
		 * isClosed() - returns true if the socket is closed (false if open).
		 * !isClosed() - returns true if the socket is open (false if closed).
		 */
	}
	
	/**
	 * Returns the user's ID
	 * @return the ID of the user
	 */
	public int getUserId() {
		return this.id;
	}
	
	/**
	 * Returns the the user's nickname
	 * @return the display name of the user
	 */
	public String getUserName() {
		return this.nickname;
	}
	
	/**
	 * Displays a message to the user in no specific channel
	 * @param message the message to send
	 * @return if the message was delivered
	 * @throws IOException 
	 */
	public boolean deliverMessage(String message, String sender) throws IOException {
		// Format: "<user> The quick brown fox"
		if (isActive()) {
			out.writeUTF("<" + sender + "> " + message);
			out.flush();
			return true;
		}
		return false;
	}
	
	/**
	 * Displays a message to the user in a specific channel<br>
	 * Note: if a user is not in the channel, the message will not be shown
	 * @param channel the channel in which to display the message
	 * @param message the message to send
	 * @return if the message was delivered to the client
	 * @throws IOException 
	 */
	public boolean deliverMessage(String channel, String message, String sender) throws IOException {
		// Format: "#main <user> The quick brown fox"
		if (isActive()) {
			out.writeUTF(channel + " <" + sender + "> " + message);
			out.flush();
			return true;
		}
		return false;
	}
	
	/**
	 * Sends a message to the user, indicating it is a private message
	 * Will always be displayed to the user, in whatever channel they happen to be in
	 * @param message the message to send to the user
	 * @return true if the message was delivered
	 * @throws IOException 
	 */
	public boolean deliverPrivately(String message, String sender) throws IOException {
		// Format: "[Whisper] <user> The quick brown fox"
		return deliverPrivately(message, sender, true);
	}
	
	/**
	 * Sends a message to the user, indicating it is a private message
	 * @param message the message to send to the user
	 * @param display if the message should be shown to the user
	 * @return true if the message was delivered
	 * @throws IOException 
	 */
	public boolean deliverPrivately(String message, String sender, boolean display) throws IOException {
		if (isActive()) {
			if (display) { // Private message for client to show
				// Format: "[Whisper] <user> The quick brown fox"
				out.writeUTF("[Whisper] <" + sender + "> " + message);
				out.flush();
			} else { // Private message for client to not show
				// Format: "[Hidden] <System> USERLIST: [user_1,user_2,user_3]"
				out.writeUTF("[Hidden] <" + sender + "> " + message);
				out.flush();
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Echoes a message back to the user with their username.
	 * This is intended to be used to show the user what commands
	 * they typed, e.g. "<User1> /users", so they can easily see syntax errors.
	 * @param message the message the user sent
	 * @return true if the message was delivered
	 * @throws IOException
	 */
	public boolean echo(String message) throws IOException {
		return echo(message, nickname);
	}
	
	/**
	 * Echoes a message back to the user with their username.
	 * This is intended to be used to show the user what commands
	 * they typed, e.g. "<User1> /users", so they can easily see syntax errors.
	 * This also has a nick parameter so you can display the older name of a user
	 * if they are changing their name.
	 * @param message the message the user sent
	 * @param nick the nickname to display
	 * @return true if the message was delivered
	 * @throws IOException
	 */
	public boolean echo(String message, String nick) throws IOException {
		if (isActive()) {
			out.writeUTF("<" + nick + "> " + message);
			out.flush();
			return true;
		}
		return false;
	}
	
	/**
	 * Client listener thread
	 */
	public void run() {
		try {
			
			// Join message
			Server.broadcastTo("Welcome to the chat server, your ID is: " + id + "\n" +
								"You have connected to port " + socket.getPort() + ".\n" +
								"View available commands with \"/help\"", this.getUserName(), Server.SYSTEM_NAME);
			Server.joinChannel(Server.DEFAULT_CHANNEL, this.getUserName());
			
			String input = "";
			do {
				input = in.readUTF();
				if (input.startsWith("/")) {
					// Split message up
					String[] splitInput = input.split("\\s+");
					String currentName = nickname;
					if (!input.contains(" -q")) {
						// The command was not executed with a "quiet" output request
						echo(input, currentName);
						String commandOutput = runCommand(splitInput);
						if (commandOutput.startsWith("&&") && commandOutput.indexOf("|") > 2) {
							// This message has output to send hidden and output to display to the user
							// e.g. "&&CHANJOIN: #secretclub|Joining channel #secretclub..."
							Server.broadcastTo(commandOutput.substring(2, commandOutput.indexOf("|")), nickname, Server.SYSTEM_NAME, false);
							Server.broadcastTo(commandOutput.substring(commandOutput.indexOf("|") + 1), nickname, Server.SYSTEM_NAME);
						} else {
							Server.broadcastTo(commandOutput, nickname, Server.SYSTEM_NAME);
						}
					} else { // The command will be executed quietly
						String commandOutput = runCommand(splitInput);
						Server.broadcastTo(commandOutput, nickname, Server.SYSTEM_NAME, false);
					}
				} else if (input.startsWith("#")) {
					//System.out.println("Message to broadcast: <" + id + "> " + input);
					String channel = input.substring(0, input.indexOf(" "));
					input = input.substring(input.indexOf(" ") + 1);
					Server.broadcast(input, nickname, channel);
				} else {
					//System.out.println("Message to broadcast: <" + id + "> " + input);
					Server.broadcast(input, nickname);
				}
			} while (!(Server.quitCommands.contains(input.toLowerCase())));
			Server.leaveAllChannels(this.getUserName());
			System.out.println("Client " + nickname + " (ID " + id + ") has disconnected gracefully.");
			Server.broadcast(nickname + " has disconnected (client quit)", Server.SYSTEM_NAME);
			socket.close();
			if (!Server.discardThread(this)) {
				System.err.println("Error discarding client thread " + id + " (name: " + this.nickname + ")");
			}
			return;
		} catch (IOException e) {
			System.err.println("Error in client thread " + id + ":\n" + e.getMessage());
			
			System.err.println("Assuming client " + nickname + " (ID " + id + ") has disconnected forcefully.");
			Server.leaveAllChannels(this.getUserName());
			if (Server.discardThread(this)) {
				System.err.println("Successfully discarded client thread " + id + " (name: " + this.nickname + ")");
				try {
					Server.broadcast(nickname + " has disconnected (server exception)", Server.SYSTEM_NAME);
				} catch (IOException e1) {
					// Yeah, I know this is an exception inside an exception.
					// But I need to notify the other clients if one disconnects unexpectedly.
					// I put this inside this if statement because if this exception is thrown
					// AND the server successfully removed the client from the active connections
					// list, then it should be safe to notify the other clients.
					System.err.println("Error notifying other clients about client " + id + " (name: " + this.nickname + ") disconnecting:");
					System.err.println(e1.getMessage());
				}
			} else {
				System.err.println("Error discarding client thread " + id + " (name: " + this.nickname + ")");
			}
		}
	}
	
	/**
	 * Dispatches commands to the appropriate methods.
	 * @param command the user's full message
	 * @return command output
	 */
	private String runCommand(String[] command) {
		switch(command[0].toLowerCase()) {
			case "/channels":
				return commandListChannels(command);
			case "/nick":
				return commandChangeNick(command);
			case "/help":
				return commandHelp(command);
			case "/join":
				return commandJoin(command);
			case "/leave":
				return commandLeave(command);
			case "/ping":
				return commandPing(command);
			case "/users":
				return commandListUsers(command);
			case "/w":
			case "/whisper":
				return commandWhisper(command);
			case "/close":
			case "/disconnect":
			case "/exit":
			case "/quit":
				return commandQuit(command);
			default:
				return commandUnknown(command);
		}
	}
	
	/**
	 * Changes the nickname of the user
	 * @param command
	 * @return
	 */
	private String commandChangeNick(String[] command) {
		if (command.length != 2) {
			return "Nick: Invalid syntax. Usage: \"/nick [name]\" (changes your display name; must be alphanumeric characters)";
		} else if (Server.reservedNames.contains(command[1].toLowerCase())) {
			return "Nick: Invalid name - reserved for server use";
		} else if (Server.searchForUser(command[1])) {
			return "Nick: Name is already in use";
		} else if (!command[1].matches(Server.USERNAME_REGEX)) {
			return "Nick: Invalid name - must be alphanumeric characters without whitespace";
		} else {
			try {
				if (!Server.updateName(this.getUserName(), command[1])) {
					Server.updateName(command[1], this.getUserName()); // Attempt to revert name change
					return "Nick: Name change failed due to internal server error. Please try again.";
				}
				Server.broadcast(nickname + " is now known as " + command[1], Server.SYSTEM_NAME);
				this.nickname = command[1];
			} catch (IOException e) {
				System.err.println("Error sending nick change notification for " + nickname + " (client " + id + ") to all clients");
				e.printStackTrace();
				return "Nick: Name change failed due to internal server error. Please try again.";
			}
			return "Nick: Your nickname has been updated";
		}
	}
	
	/**
	 * Prints the help message
	 * @param command
	 * @return
	 */
	private String commandHelp(String[] command) {
		return "Help: Available commands:\n" +
				"/channels - List of active channels\n" +
				"/close, /disconnect, /exit, /quit - Disconnect from the server\n" +
				"/help - List of available commands\n" +
				"/join - Join a channel (or create a new one)\n" +
				"/leave - Leave a channel\n" +
				"/nick - Change your display name\n" +
				"/ping - Test your connection\n" +
				"/users - Lists the connected uers\n" +
				"/w, /whisper - Send private messages to other users";
	}
	
	/**
	 * Joins a new channel
	 * @param command
	 * @return
	 */
	private String commandJoin(String[] command) {
		if (command.length == 1 || (command.length > 2 && !command[2].equals("-q"))) {
			return "Join: Invalid syntax. Usage: \"/join [channel]\" (joins a channel; if the channel does not exist, it will be created)";
		}
		
		if (!command[1].matches(Server.CHANNEL_REGEX)) {
			// Channel name invalid
			return "Join: Invalid channel name. (Must start with a # and only be lowercase alphanumeric characters.)";
		}
		
		if (Server.hasChannel(command[1])) {
			// Channel exists, join it
			if (!Server.joinChannel(command[1], this.getUserName())) {
				// If false, user is probably already in the channel.
				return "Join: Unable to create join channel " + command[1] + " - user already present.";
			}
		} else {
			// Channel does not exist, create it
			if (!Server.createChannel(command[1], this.getUserName())) { // This auto-joins the user
				return "Join: Unable to create join channel " + command[1] + " - server error occured.";
			}
		}
		
		if (command.length == 3 && command[2].equals("-q")) {
			return "CHANJOIN: " + command[1];
		}
		return "&&CHANJOIN: " + command[1] + "|Joining channel " + command[1] + "...";
	}
	
	/**
	 * Leaves a channel
	 * @param command
	 * @return
	 */
	private String commandLeave(String[] command) {
		if (command.length == 1 || (command.length > 2 && !command[2].equals("-q"))) {
			return "Leave: Invalid syntax. Usage: \"/leave [channel]\" (leaves a channel)";
		}
		
		if (!command[1].matches(Server.CHANNEL_REGEX)) {
			// Channel name invalid
			return "Leave: Invalid channel name. (Must start with a # and only be lowercase alphanumeric characters.)";
		}
		
		if (command.length == 2 && command[1].equals(Server.DEFAULT_CHANNEL)) {
			return "Leave: Cannot disconnect from " + command[1] + ".";
		} else if (command.length == 3 && command[1].equals(Server.DEFAULT_CHANNEL) && command[2].equals("-q")) {
			return "CHANLEAVE: " + command[1] + " 403";
		}
		
		if (Server.hasChannel(command[1])) {
			// Channel exists
			if (Server.leaveChannel(command[1], this.getUserName())) {
				// User was joined
				if (command.length == 3 && command[2].equals("-q")) {
					return "CHANLEAVE: " + command[1];
				}
				return "&&CHANLEAVE: " + command[1] + "|Leaving channel " + command[1] + "...";
			} else {
				// Unable to leave channel - probably not joined
				if (command.length == 3 && command[2].equals("-q")) {
					return "CHANLEAVE: " + command[1] + " 400";
				}
				return "Leave: User was not in the specified channel";
			}
		} else {
			// Channel does not exist
			if (command.length == 2 && command[2].equals("-q")) {
				return "CHANLEAVE: " + command[1] + " 404";
			}
			return "Leave: Channel not found";
		}
	}
	
	/**
	 * Connection test
	 * @param command
	 * @return
	 */
	private String commandPing(String[] command) {
		if (command.length == 1) {
			return "Ping: Pong!";
		} else {
			return "Ping: Invalid syntax. Usage: \"/ping\" (tests the connection to the server by replying with \"Pong!\")";
		}
	}
	
	/**
	 * Disconnects from the server
	 * @param command
	 * @return
	 */
	private String commandQuit(String[] command) {
		String quitCmdUsed = Character.toUpperCase(command[0].charAt(1)) + command[0].substring(2);
		/* In the other commands, I do "Command: [message]", but quitting has multiple aliases,
		 * so I use this to get the alias used and capitalize the first letter (e.g. "/quit" -> "Quit").
		 */
		if (command.length == 1) {
			return quitCmdUsed + ": Closing connection...";
		} else {
			return quitCmdUsed + ": Invalid syntax. Usage: \"/" + quitCmdUsed.toLowerCase() + "\" (closes the connection to the server)";
		}
	}
	
	/**
	 * Lists all active channels on the server
	 * @param command
	 * @return
	 */
	private String commandListChannels(String[] command) {
		if (command.length == 1) {
			// Format: "Channels: #chan1, #chan2, #main, #xkcd"
			return "Channels: " + Server.getChannels(true);
		} else if (command.length == 2 && command[1].equals("-c")) {
			// Format: "Your Channels: #chan1, #chan2, #main, #xkcd"
			return "Your Channels: " + Server.getChannels(true, this.getUserName());
		} else if (command.length == 2 && command[1].equals("-q")) {
			// Format: "CHANLIST: [#chan1 #chan2 #main #xkcd]"
			// -q implies -c because only the client should be issuing -q's
			return "CHANLIST: " + Server.getChannels(false, this.getUserName());
		} else {
			return "Channels: Invalid syntax. Usage: \"/channels [-c]\" (lists all active channels on the server; use -c for only your connected channels)";
		}
	}
	
	/**
	 * Lists all users on the server or in a specific channel
	 * @param command
	 * @return
	 */
	private String commandListUsers(String[] command) {
		if (command.length == 1) {
			// Format: "Users: admin, user1, user5, zebra"
			return "Users: " + Server.getConnectedUsers(true);
		} else if (command.length == 2 && command[1].equals("-q")) {
			// Format: "USERLIST: [admin user1 user2 zebra]"
			return "USERLIST: " + Server.getConnectedUsers(false);
		} else if (command.length == 2) {
			// User wants to list the users in a channel
			if (Server.hasChannel(command[1])) {
				return "Users in " + command[1] + ": " + Server.getConnectedUsers(true, command[1]);
			}
			return "Users: Channel not found";
		} else if (command.length == 3 && command[2].equals("-q")) {
			// Client wants to list the users in a channel
			if (Server.hasChannel(command[1])) {
				return "USERLIST: " + Server.getConnectedUsers(false, command[1]);
			}
			return "USERLIST: " + command[1] + " 404";
		} else {
			return "Users: Invalid syntax. Usage: \"/users [channel]\" (lists the currently connected users on the server or a channel (if a channel name is given))";
		}
	}
	
	/**
	 * Send a private message to another user
	 * @param command
	 * @return
	 */
	private String commandWhisper(String[] command) {
		//String aliasUsed = Character.toUpperCase(command[0].charAt(1)) + command[0].substring(2);
		// No alias needed here, the only alises are (currently) "/w" and "/whisper" - saying
		// "W: Invalid syntax." wouldn't make much sense, but using Whisper for both does.
		if (command.length < 3) {
			return "Whisper: Invalid syntax. Usage: \"whisper [user] [message]\" (sends a message only visible to [user])";
		} else {
			try {
				if (Server.broadcastTo(String.join(" ", Arrays.asList(command).subList(2, command.length)), command[1], nickname)) {
					return "Whisper: Message sent.";
				} else {
					return "Whisper: User \"" + command[1] + "\" not found.";
				}
			} catch (IOException e) {
				System.err.println("Error sending whisper from " + nickname + " (client " + id + ") to "
									+ command[1] + " (client " + Server.getIdOfUser(command[1]) + "):");
				e.printStackTrace();
				return "Whisper: Message failed to send.";
			}
		}
	}
	
	/**
	 * Invalid command
	 * @param command
	 * @return
	 */
	private String commandUnknown(String[] command) {
		return "Unrecognized command: \"" + command[0] + "\". Type \"/help\" for a list of commands.";
	}
}
