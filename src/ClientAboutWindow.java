import javax.swing.JFrame;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;

public class ClientAboutWindow {

	private JFrame frmAboutSocket;
	private JButton btnClose;
	private JScrollPane scrollPane;
	private JTextPane txtpnDetails;
	private JLabel lblAboutThisApplication;

	/**
	 * Create the application.
	 */
	public ClientAboutWindow() {
		initialize();
		frmAboutSocket.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAboutSocket = new JFrame();
		frmAboutSocket.setTitle("About 3166 Socket Chat");
		frmAboutSocket.setBounds(100, 100, 450, 300);
		frmAboutSocket.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
		frmAboutSocket.getContentPane().setLayout(gridBagLayout);
		
		lblAboutThisApplication = new JLabel("About this Application");
		GridBagConstraints gbc_lblAboutThisApplication = new GridBagConstraints();
		gbc_lblAboutThisApplication.gridwidth = 3;
		gbc_lblAboutThisApplication.insets = new Insets(0, 0, 5, 5);
		gbc_lblAboutThisApplication.gridx = 0;
		gbc_lblAboutThisApplication.gridy = 0;
		frmAboutSocket.getContentPane().add(lblAboutThisApplication, gbc_lblAboutThisApplication);
		
		scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 3;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		frmAboutSocket.getContentPane().add(scrollPane, gbc_scrollPane);
		
		String infoPane = "This application was written for ITSC 3166 - Intro to Computer Networking at UNC Charlotte.\n" +
				"\nGroup Members: David Lucadou, Elijah Leatherwood, Matt Bavis, Shiv Parekh";
		txtpnDetails = new JTextPane();
		txtpnDetails.setText(infoPane);
		txtpnDetails.setEditable(false);
		scrollPane.setViewportView(txtpnDetails);
		
		btnClose = new JButton("Close");
		btnClose.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				frmAboutSocket.dispose();
			}
		});
		GridBagConstraints gbc_btnClose = new GridBagConstraints();
		gbc_btnClose.gridwidth = 3;
		gbc_btnClose.gridx = 0;
		gbc_btnClose.gridy = 2;
		frmAboutSocket.getContentPane().add(btnClose, gbc_btnClose);
	}
}
