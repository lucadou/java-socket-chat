import java.net.*;
import java.util.ArrayList;
import java.util.stream.Collectors;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.DefaultCaret;

import net.miginfocom.swing.MigLayout;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;

public class Client extends Thread {
	private static Socket socket;
	private static DataOutputStream out;
	private static Client chatGui;
	private static Thread chatListener;
	private static ClientThread chatClient;
	private String server;
	private int port;
	private String username;
	private static ClientChannel activeChannel;
	private static ArrayList<ClientChannel> connectedChannels;
	private static ArrayList<String> initMessages;
	
	private JFrame frmSocketChat;
	private JScrollPane channelListPane;
	private JScrollPane messageListPane;
	private JScrollPane userListPane;
	private JList<String> channelList;
	private DefaultListModel<String> channelListModel;
	private JTextArea messageList;
	private JTextArea userList;
	private JTextField txtMessageEditBox;
	
	/**
	 * Create the application.
	 */
	public Client() {
		initialize();
		frmSocketChat.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSocketChat = new JFrame();
		frmSocketChat.setTitle("3166 Socket Chat");
		frmSocketChat.setBounds(100, 100, 640, 480);
		frmSocketChat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSocketChat.getContentPane().setLayout(new MigLayout("", "[75%,grow][][grow]", "[grow][][grow][]"));
		frmSocketChat.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				// Gracefully disconnect when the user hits the X button (rather than close the connection abruptly)
				if (chatClient != null && chatClient.isConnected()) {
					Client.disconnect();
				}
			}
		});
		
		messageList = new JTextArea();
		messageList.setLineWrap(true);
		messageList.setWrapStyleWord(true);
		messageList.setEditable(false);
		messageListPane = new JScrollPane(messageList);
		messageListPane.setViewportView(messageList);
		
		// Set pane to auto-scroll when new messages are added
		DefaultCaret messageListPaneCaret = (DefaultCaret)messageList.getCaret();
		messageListPaneCaret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		frmSocketChat.getContentPane().add(messageListPane, "cell 0 0 1 3,grow");
		
		JLabel lblChannels = new JLabel("Channels");
		frmSocketChat.getContentPane().add(lblChannels, "flowy,cell 2 0");
		channelListPane = new JScrollPane();
		frmSocketChat.getContentPane().add(channelListPane, "cell 2 0,grow");
		
		channelList = new JList<String>(new DefaultListModel<String>());
		channelList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				//System.out.println("[Debug] Selected: " + channelList.getSelectedIndex());
				String selectedChannel = (String)channelList.getSelectedValue();
				if (selectedChannel != null && !Client.activeChannel.getName().equals(selectedChannel)) {
					//System.out.println("Selected value change to: " + selectedChannel + " ID: " + newIndex);
					chatGui.setActiveChannel(selectedChannel, chatGui.getChannelListID(selectedChannel));
					Client.activeChannel.clearUnreads();
				}
			}
		});
		channelList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		channelList.setVisibleRowCount(4);
		channelListPane.setViewportView(channelList);
		channelList.setCellRenderer(new DefaultListCellRenderer() {
			private static final long serialVersionUID = 1L;

			@SuppressWarnings("rawtypes")
			@Override
			public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
				Component c = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
				setText((String) value);
				if (Client.hasChannel((String) value) && Client.getChannel((String) value).hasUnreads()) {
					setForeground(Color.RED);
				} else {
					setForeground(Color.BLACK);
				}
				return c;
			}
		});
		
		txtMessageEditBox = new JTextField();
		txtMessageEditBox.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					sendMessage();
				}
			}
		});
		
		JLabel lblUsers = new JLabel("Online Users");
		frmSocketChat.getContentPane().add(lblUsers, "cell 2 1");
		
		userListPane = new JScrollPane();
		frmSocketChat.getContentPane().add(userListPane, "cell 2 2,grow");
		
		userList = new JTextArea();
		userList.setLineWrap(true);
		userList.setWrapStyleWord(true);
		userListPane.setViewportView(userList);
		userList.setEditable(false);
		txtMessageEditBox.setToolTipText("Type your message here (press enter to send)");
		frmSocketChat.getContentPane().add(txtMessageEditBox, "cell 0 3,growx");
		txtMessageEditBox.setColumns(10);
		
		JButton btnSend = new JButton("Send");
		btnSend.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					sendMessage();
				}
			}
		});
		btnSend.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				sendMessage();
			}
		});
		frmSocketChat.getContentPane().add(btnSend, "cell 2 3");
		
		JMenuBar menuBar = new JMenuBar();
		frmSocketChat.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmConnect = new JMenuItem("Connect");
		mntmConnect.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				new ClientConnectWindow(server, "" + port, username, false);
			}
		});
		mnFile.add(mntmConnect);
		
		JMenuItem mntmDisconnect = new JMenuItem("Disconnect");
		mntmDisconnect.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				if (chatClient != null && chatClient.isConnected()) {
					Client.disconnect();
				} else {
					Client.displayMessage("Error disconnecting: you are not connected to any server");
				}
			}
		});
		mnFile.add(mntmDisconnect);
		
		JMenuItem mntmReconnect = new JMenuItem("Reconnect");
		mntmReconnect.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				Client.connect();
			}
		});
		mnFile.add(mntmReconnect);
		
		JMenuItem mntmQuit = new JMenuItem("Quit");
		mntmQuit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				// Disconnect and quit
				if (chatClient != null && chatClient.isConnected()) {
					sendMessage("/disconnect");
				}
				System.exit(0);
			}
		});
		mnFile.add(mntmQuit);
		
		JMenu mnHelp = new JMenu("About");
		menuBar.add(mnHelp);
		
		JMenuItem mntmAbout = new JMenuItem("About");
		mntmAbout.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
				new ClientAboutWindow();
			}
		});
		mnHelp.add(mntmAbout);
	}
	
	/**
	 * Posts a message to the GUI window. Intended to be called when you know the GUI is initialized.
	 * @param message the message to append to the message list in the GUI
	 */
	public void addMessage(String message) {
		this.addMessage(message, true);
	}
	
	/**
	 * Posts a message to the GUI window. Intended to be called when you know the GUI is initialized.
	 * @param message the message to append to the message list in the GUI
	 * @param storeMessage if the message should be stored in the channel's message list
	 */
	public void addMessage(String message, boolean storeMessage) {
		//System.out.println("[Debug] Message received: " + message);
		if (message.startsWith("[Hidden]")) {
			if (message.contains("USERLIST")) {
				this.updateUsers(message);
			} else if (message.contains("CHANLIST")) {
				this.updateChannels(message);
			} else if (message.contains("CHANJOIN")) {
				// Format: "[Hidden] CHANJOIN: #chan"
				if (message.substring(message.indexOf("#")).contains(" ")) {
					// Indicates an error occurred
					// Format: "[Hidden] CHANJOIN: #chan 500"
					String channel = message.substring(message.indexOf("#"), message.indexOf(" ", message.indexOf("#")));
					String error = message.substring(message.indexOf(" ", message.indexOf("#")) + 1);
					System.err.println("Error joining channel " + channel + ": " + error);
				} else {
					Client.joinChannel(message.substring(message.indexOf("#")));
				}
			} else if (message.contains("CHANLEAVE")) {
				// Format: "[Hidden] CHANLEAVE: #chan"
				if (message.substring(message.indexOf("#")).contains(" ")) {
					// Indicates an error occurred
					// Format: "[Hidden] CHANJOIN: #chan 500"
					String channel = message.substring(message.indexOf("#"), message.indexOf(" ", message.indexOf("#")));
					String error = message.substring(message.indexOf(" ", message.indexOf("#")) + 1);
					System.err.println("Error leaving channel " + channel + ": " + error);
				} else {
					Client.leaveChannel(message.substring(message.indexOf("#")));
				}
			}
		} else if (message.startsWith("#")) {
			String channel = message.substring(0, message.indexOf(" "));
			message = message.substring(message.indexOf(" ") + 1);
			
			Client.getChannel(channel).addMessage(message);
			if (channel.equals(Client.activeChannel.getName())) {
				// Channel is active, display message to user
				messageList.append(message + "\n");
			}
			/* Because I created a custom DefaultListCellRenderer, the list item will automatically turn red
			 * for unread messages. I didn't need to add a ListDataListener because the channel list gets
			 * updated every 10 seconds or so. The channel list is wiped and instantly repopulated, so the
			 * DefaultListCellRenderer sets the color to red whenever the updateChanels method is called.
			 * When the user clicks on a channel, the color turns black again. I can't figure out exactly why.
			 * It might have something to do with when this.setActiveChannel calls this.channelList.setSelectedIndex?
			 * Setting the selected index does clear the unreads, and it might call the DefaultListCellRenderer again
			 * to get the colors when it is selected?
			 * This is all conjecture: Digging around the Java source code, I believe it may be rerendered when the
			 * PropertyChangeEvent is fired when the object is selected, so my DefaultListCellRenderer gets re-run.
			 * I have no idea how accurate that statement is, though I will say this: I am in awe at the Swing
			 * source code. So much planning went into it, it's beyond anything I could have ever dreamed of. I
			 * never realized just how much work would go into a GUI framework.
			 */
		} else {
			// These will be whispers, user connects/disconnects, etc.
			if (Client.initMessages != null) { // True when the client is starting up and a channel has not yet been joined
				Client.initMessages.add(message);
			} else if (Client.hasChannel(Server.DEFAULT_CHANNEL)) {
				/* Save in both the active channel and the default channel so the user would not be confused
				 * if some messages disappeared when they changed to another channel, then changed back,
				 * and they can go to #main and view all whispers, connects/disconnects, etc.
				 */
				if (!Client.getChannel(Server.DEFAULT_CHANNEL).getMessages().contains(message) && storeMessage) {
					Client.getChannel(Server.DEFAULT_CHANNEL).addMessage(message, false);
				}
				if (Client.activeChannel != null && !Client.activeChannel.getName().equals(Server.DEFAULT_CHANNEL) && storeMessage) {
					Client.activeChannel.addMessage(message);
					//System.out.println("[Debug] Channel: " + Client.activeChannel.getName() + " is storing message: " + message);
				}
			}
			messageList.append(message + "\n");
		}
	}
	
	/**
	 * Creates a channel
	 * @param name the name of the channel
	 */
	public void addChannel(String name) {
		Client.connectedChannels.add(new ClientChannel(name));
	}
	
	/**
	 * Removes all messages from the GUI
	 */
	public void clearMessages() {
		//System.out.println("Clearing messages in channel " + Client.activeChannel.getName());
		this.messageList.setText("");
	}
	
	/**
	 * Gets the index of a channel in the channelList (in the GUI)
	 * @param channel the name of the channel to find
	 * @return the index of the channel, or -1 if not found
	 */
	public int getChannelListID(String channel) {
		 for(int i = 0; i < channelList.getModel().getSize(); i++) {
			 //System.out.println("[Debug] Element " + i + ": " + channelList.getModel().getElementAt(i));
			 if (channel.equals(channelList.getModel().getElementAt(i))) {
				 return i;
			 }
		}
		return -1;
	}
	
	/**
	 * Joins a channel, makes it the active channel, and changes the GUI client window to it
	 * @param channel the name of the channel
	 */
	public static void joinChannel(String channel) {
		Client.joinChannel(channel, null);
	}
	
	/**
	 * Joins a channel, makes it the active channel, and changes the GUI client window to it
	 * @param channel the name of the channels
	 * @param messages an ArrayList of messages to pre-populate in the channel (ignored if null)
	 */
	public static void joinChannel(String channel, ArrayList<String> messages) {
		//System.out.println("[Debug] Joining channel: " + channel);
		ClientChannel cc;
		if (!Client.hasChannel(channel)) {
			if (messages == null) {
				cc = new ClientChannel(channel);
			} else {
				cc = new ClientChannel(channel, messages);
			}
			Client.connectedChannels.add(cc);
		} else {
			cc = Client.getChannel(channel);
		}
		chatGui.updateChannels(); // Refresh channel list
		
		chatGui.setActiveChannel(cc, chatGui.getChannelListID(channel));
		//System.out.println("[Debug] Channel ID in GUI list is: " + chatGui.getChannelListID(channel));
	}
	
	/**
	 * Leaves a channel and changes the active channel to the main channel if it is the current channel.<br>
	 * Note: this assumes the default channel cannot be left.
	 * @param channel the name of the channel
	 */
	public static void leaveChannel(String channel) {
		if (Client.activeChannel.getName().equals(channel)) {
			chatGui.setActiveChannel(Server.DEFAULT_CHANNEL, chatGui.getChannelListID(Server.DEFAULT_CHANNEL));
		}
		chatGui.updateChannels();
	}
	
	/**
	 * Sets the active channel and changes the GUI client window
	 * @param channel the name of the channel
	 */
	public void setActiveChannel(ClientChannel channel, int index) {
		Client.activeChannel = channel;
		// Clear unread messages
		channel.clearUnreads();
		// Change messageList
		this.clearMessages();
		String[] receivedMessages = (String[]) channel.getMessages().toArray(new String[channel.getMessages().size()]);
			// Casting to array before displaying messages prevents items from being duplicated when calling displayMessages
		for (int i = 0; i < receivedMessages.length; i++) {
			Client.displayMessage(receivedMessages[i], false);
		}
		// Change userList
		chatGui.updateUsers();
		// Highlight in channelList
		if (index != -1) {
			this.channelList.setSelectedIndex(index);
		}
		chatGui.txtMessageEditBox.grabFocus(); // Give focus to the message box so the user can start typing immediately
	}
	
	/**
	 * Sets the active channel and changes the GUI client window
	 * @param channel the name of the channel
	 */
	public void setActiveChannel(String channel, int index) {
		ClientChannel cc = Client.getChannel(channel);
		this.setActiveChannel(cc, index);
	}
	
	/**
	 * Gets the channel currently visible to the user.
	 * @return the name of the channel
	 */
	public static ClientChannel getActiveChannel() {
		return Client.activeChannel;
	}
	
	/**
	 * Gets the name of the channel currently visible to the user.
	 * @return the name of the channel
	 */
	public static String getActiveChannelName() {
		return Client.getActiveChannel().getName();
	}
	
	/**
	 * Gets the ClientChannel object with a given name
	 * @param channel the channel to load
	 * @return the ClientChannel, or null if it was not found
	 */
	public static ClientChannel getChannel(String channel) {
		for (ClientChannel chan : connectedChannels) {
			if (chan.getName().equals(channel)) {
				return chan;
			}
		}
		return null;
	}
	
	/**
	 * Posts a message to the GUI window if it is active, or the console if the GUI is not yet active.
	 * Use this if you know the GUI is not yet active or have any doubts about the GUI being active.
	 * @param message the message to display to the user (or append to the message list in the GUI)
	 */
	public static void displayMessage(String message) {
		if (chatGui != null) {
			chatGui.addMessage(message, true);
		} else {
			System.out.println(message);
		}
	}
	
	/**
	 * Posts a message to the GUI window if it is active, or the console if the GUI is not yet active.
	 * Use this if you know the GUI is not yet active or have any doubts about the GUI being active.
	 * @param message the message to display to the user (or append to the message list in the GUI)
	 * @param saveMessage if the message should be saved in the channel's message list
	 */
	public static void displayMessage(String message, boolean saveMessage) {
		if (chatGui != null) {
			chatGui.addMessage(message, saveMessage);
		} else {
			System.out.println(message);
		}
	}
	
	/**
	 * Determines if a channel has been joined by name
	 * @param channel the channel to look for
	 * @return true if the channel exists and has been joined, false if the channel has not been joined
	 * (though the channel may be active on the server)
	 */
	public static boolean hasChannel(String channel) {
		return Client.channelID(channel) != -1;
	}
	
	/**
	 * Returns the index of the ClientChannel in the connectedClients ArrayList.<br>
	 * Note: this is <i>NOT</i> the ID of the channel in the channelList.
	 * @param channel the channel to check for
	 * @return the index of the client, or -1 if it was not found
	 */
	public static int channelID(String channel) {
		ArrayList<String> channels = connectedChannels.stream()
										.map(ClientChannel::getName)
										.collect(Collectors.toCollection(ArrayList::new));
		return channels.indexOf(channel);
	}
	
	/**
	 * Sends the message in the message edit box to the chat server
	 * @return true if the message was sent
	 */
	public boolean sendMessage() {
		boolean result = this.sendMessage(txtMessageEditBox.getText());
		txtMessageEditBox.setText("");
		return result;
	}
	
	/**
	 * Sends a message to the chat server
	 * @param message the message to send
	 * @return true if the message was sent
	 */
	public boolean sendMessage(String message) {
		try {
			if (!message.startsWith("/")) {
				message = Client.getActiveChannelName() + " " + message;
			}
			out.writeUTF(message);
		} catch (IOException e) {
			this.addMessage("Failed to send message: socket error");
			System.err.println("Error sending message: " + e.getMessage());
			e.printStackTrace();
			return false;
		} catch (NullPointerException e) {
			if (chatClient == null) {
				this.addMessage("Failed to send message: not connected to a server");
			} else {
				this.addMessage("Failed to send message: unknown error");
			}
			System.err.println("Error sending message: " + e.getMessage() + " (most likely cause: not being connected)");
		}
		return true;
	}
	
	/**
	 * Gets the currently connected users and adds them to the Users list.
	 * Sets the list to blank if the client is disconnected.
	 */
	public void updateUsers() {
		if (chatClient != null && chatClient.isConnected()) {
			// Update users
			if (Client.activeChannel != null) {
				this.sendMessage("/users " + Client.activeChannel.getName() + " -q");
			} else {
				this.sendMessage("/users -q");
			}
		} else {
			// Clear list
			userList.setText("");
		}
	}
	
	/**
	 * Sets the list of connected users from a string.
	 * @param users a list of users in the format "USERLIST: [user1 user_2 xyzzyx]"
	 */
	public void updateUsers(String users) {
		users = users.substring(users.indexOf(": [") + 3, users.indexOf("]", 10));
		userList.setText(String.join("\n", users.split(" ")));
	}
	
	/**
	 * Gets the currently active channels and adds them to the Channels list.
	 * Sets the list to blank if the client is disconnected.
	 */
	public void updateChannels() {
		if (chatClient != null && chatClient.isConnected()) {
			// Update users
			//System.out.println("[Debug] Updating channels...");
			this.sendMessage("/channels -q");
		} else {
			// Clear list
			//System.out.println("[Debug] Cannot update channels");
			channelListModel = (DefaultListModel<String>) channelList.getModel();
			channelListModel.removeAllElements();
		}
	}
	
	/**
	 * Sets the list of channels a user is connected to from a string.
	 * @param channels a list of users in the format "USERLIST: [#chan1 #chan2 #main]"
	 */
	public void updateChannels(String channels) {
		//System.out.println("[Debug] Updating channels in GUI");
		channels = channels.substring(channels.indexOf(": [") + 3, channels.indexOf("]", 10));
		channelListModel = (DefaultListModel<String>) channelList.getModel();
		if (channelListModel.size() > 0) {
			channelListModel.removeRange(0, channelListModel.size() - 1);
		}
		
		String[] newChannels = channels.split(" ");
		for (int i = 0; i < newChannels.length; i++) {
			if (!Client.hasChannel(newChannels[i])) {
				chatGui.addChannel(newChannels[i]);
			}
			channelListModel.addElement(newChannels[i]);
		}
		if (Client.activeChannel != null) {
			this.channelList.setSelectedIndex(this.getChannelListID(Client.activeChannel.getName()));
		}
	}
	
	/**
	 * Starts the thread to update the user list ever 30 seconds
	 */
	@Override
	public void run() {
		while (true) {
			System.out.println("Updating user list...");
			this.updateUsers();
			System.out.println("User list updated");
			System.out.println("Updating channel list...");
			this.updateChannels();
			System.out.println("Channel list updated");
			try {
				Thread.sleep(15000); // Update every 15 seconds
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Checks if a server is probably valid IPv4/IPv6 address or domain name.
	 * This is not very accurate. Since domain names can have almost any UTF-8 character, I'm only checking if
	 * the given input contains whitespace, since that is not allowed in IPv4, IPv6, or any DNS system.
	 * @param address the address to check
	 * @return true if the address is probably a valid IPv4/IPv6/domain name
	 */
	public static boolean isAddressValid(String address) {
		if (address.length() < 3) {
			// The shorted possible domain name is 4 characters, i.e. a.co.
			// The shortest possible IPv6 address is 3 characters, i.e. ::1
			return false;
		}
		for (int i = 0; i < address.length(); i++) {
			if (Character.isWhitespace(address.charAt(i))) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Checks if a port number is valid (must be a whole number in the range [1, 65,535])
	 * @param port the port number to check
	 * @return true if the port is valid
	 */
	public static boolean isPortValid(String port) {
		int portNum = 0;
		try {
			portNum = Integer.parseInt(port);
		} catch (NumberFormatException e) {
			return false;
		}
		return portNum > 0 && portNum < 65536;
	}
	
	/**
	 * Checks if a username matches the Server's username requirements. Does not check if the username is currently in use.
	 * @param username the username to check
	 * @return true if the username is in a valid format
	 */
	public static boolean isUsernameValid(String username) {
		return username.length() == 0 || username.matches(Server.USERNAME_REGEX);
	}
	
	/**
	 * Changes the server details (for changing servers)
	 * @param server the new server
	 * @param port the new port
	 * @param username the new username
	 */
	public static void changeServerDetails(String server, int port, String username) {
		if (chatGui != null) {
			chatGui.server = server;
			chatGui.port = port;
			chatGui.username = username;
		} else {
			Client.displayMessage("Unable to update server details");
		}
	}
	
	/**
	 * Starts the chat client
	 * @param args
	 */
	public static void main(String[] args) {
		// Start GUI
		chatGui = new Client();
		chatGui.start();
		
		chatGui.server = "127.0.0.1";
		chatGui.port = 5000;
		chatGui.username = "";
		Client.connectedChannels = new ArrayList<ClientChannel>();
		Client.initMessages = new ArrayList<String>();
		new ClientConnectWindow(chatGui.server, "" + chatGui.port, chatGui.username, false);
	}
	
	/**
	 * Connects the chat client to the server
	 */
	public static void connect() {
		// Connect to server
		try {
			// Check if currently connected
			if (chatClient != null && chatClient.isConnected()) {
				Client.disconnect();
			}
			Client.displayMessage("Connecting to " + chatGui.server + " on port " + chatGui.port + "...");
			socket = new Socket(chatGui.server, chatGui.port);
			out = new DataOutputStream(socket.getOutputStream());
			Client.displayMessage("Connected");
			
			chatClient = new ClientThread(socket, chatGui);
			chatListener = new Thread(chatClient);
			chatListener.start();
			chatGui.updateUsers();
			if (chatGui.username.length() > 0) {
				chatGui.addMessage("Setting username...");
				chatGui.sendMessage("/nick " + chatGui.username);
			}
			chatGui.updateChannels();
			Client.joinChannel(Server.DEFAULT_CHANNEL, initMessages);
			initMessages = null;
			chatGui.setActiveChannel(Server.DEFAULT_CHANNEL, chatGui.getChannelListID(Server.DEFAULT_CHANNEL));
		} catch (ConnectException e) {
			Client.displayMessage("Error to connect to the server: server does not appear to be online");
			Client.displayMessage("To retry connecting to this server, go to File -> Reconnect");
			Client.displayMessage("To connect to a different server, go to File -> Connect");
		} catch (UnknownHostException e) {
			Client.displayMessage("Unknown host: " + chatGui.server);
		} catch (IOException e) {
			Client.displayMessage("Error connecting to server: " + e.getMessage());
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * Disconnects the chat clients from the server
	 */
	public static void disconnect() {
		if (chatClient != null && chatClient.isConnected()) {
			chatGui.sendMessage("/disconnect");
			try {
				Thread.sleep(100);
				/* Sleep 0.1 seconds so the "Disconnected" message will show up
				 * before the "Connecting to" and "Connected" messages without
				 * causing a noticeable delay to the user.
				 */
			} catch (InterruptedException e1) {
			}
		}
	}
}
