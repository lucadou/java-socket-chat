import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.io.*;

public class Server extends Thread {
	private static int port = 5000;
	private static int totalClients = 0;
	private static int maxClients = 10;
	private static ArrayList<ServerThread> activeClients = new ArrayList<ServerThread>();
	public static final String DEFAULT_CHANNEL = "#main";
	public static final String SYSTEM_NAME = "System";
	public static final String CHANNEL_REGEX = "^[#]([a-z0-9_-])+$";
	public static final String USERNAME_REGEX = "^[a-zA-Z0-9_-]+$";
	public static ArrayList<String> commands = new ArrayList<String>();
	public static ArrayList<String> quitCommands = new ArrayList<String>();
	public static ArrayList<String> whisperCommands = new ArrayList<String>();
	public static ArrayList<String> reservedNames = new ArrayList<String>();
	public static ArrayList<ServerChannel> channels = new ArrayList<ServerChannel>();
	
	/**
	 * Sends a message for everyone to see
	 * @param message
	 * @throws IOException 
	 */
	public static boolean broadcast(String message, String sender) throws IOException {
		for (ServerThread ct : activeClients) {
			if (ct.isActive()) {
				ct.deliverMessage(message, sender);
			}
		}
		return true;
	}
	
	/**
	 * Sends a message for everyone to see
	 * @param message
	 * @throws IOException 
	 */
	public static boolean broadcast(String message, String sender, String channel) throws IOException {
		for (ServerThread st : Server.getChannelUsers(channel)) {
			if (st.isActive()) {
				st.deliverMessage(channel, message, sender);
			}
		}
		return true;
	}
	
	/**
	 * Send a message to a specific user
	 * @param message the message to send
	 * @param recipient the user to deliver the message to
	 * @param sender the user sending the message
	 * @return if the message was delivered (false usually indicates the recipient was not found)
	 * @throws IOException 
	 */
	public static boolean broadcastTo(String message, String recipient, String sender) throws IOException {
		return broadcastTo(message, recipient, sender, true);
	}
	
	/**
	 * Sends a message to a specific user, with the option to not display the message in the client.
	 * This is useful for sending data in the background, e.g. updating the user list.
	 * @param message the message to send
	 * @param recipient the user to deliver the message to
	 * @param sender the user sending the message
	 * @param visible if the message should be displayed in the client of the recipient
	 * @return if the message was delivered (false usually indicates the recipient was not found)
	 * @throws IOException 
	 */
	public static boolean broadcastTo(String message, String recipient, String sender, boolean visible) throws IOException {
		for (ServerThread ct : activeClients) {
			if (ct.isActive() && ct.getUserName().equalsIgnoreCase(recipient)) {
				ct.deliverPrivately(message, sender, visible);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Determines if a given name is in use or not.
	 * @param name - nickname to search for
	 * @return true if the name is in use
	 */
	public static boolean searchForUser(String name) {
		for (ServerThread ct : activeClients) {
			if (ct.getUserName().equalsIgnoreCase(name)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Removes a ChatThread from the pool of active ChatThreads (do this when a client disconnects)
	 * @param ct - the ChatThread to remove
	 * @return true if the ChatThread was removed
	 */
	public static boolean discardThread(ServerThread ct) {
		return activeClients.remove(ct);
	}
	
	/**
	 * Retrieves the ID for a given username (not the active connection number)
	 * @param name the user to search for
	 * @return the ID of the user
	 */
	public static int getIdOfUser(String name) {
		for (ServerThread ct : activeClients) {
			if (ct.getUserName().equalsIgnoreCase(name)) {
				return ct.getUserId();
			}
		}
		return -1;
	}
	
	/**
	 * Gets all users connected to the server in the user-readable format [user1, user_2, user353, admin]
	 * or space-separated format [user1 user_2 user353 admin]
	 * @return the list of connected users
	 */
	public static String getConnectedUsers(boolean userReadableFormat) {
		StringBuilder users = new StringBuilder();
		if (userReadableFormat) {
			// Format: "user1, user2, user3"
			users.append(String.join(", ", activeClients.stream()
											.map(ServerThread::getUserName)
											.sorted((name1, name2) -> name1.compareTo(name2))
											.collect(Collectors.toList())
											));
		} else {
			// Format: "[user1 user2 user3]"
			users.append("[");
			users.append(String.join(" ", activeClients.stream()
											.map(ServerThread::getUserName)
											.sorted((name1, name2) -> name1.compareTo(name2))
											.collect(Collectors.toList())
											));
			users.append("]");
		}
		
		return users.toString();
	}
	
	/**
	 * Attempts to add a user to a given channel<br />
	 * Note: this does not create a channel if it does not already exist
	 * @param channel the channel to join
	 * @param user the user to add
	 * @return true if the user was added to the channel, false if the channel does not exist, is invalid, or is already joined
	 */
	public static boolean joinChannel(String channel, String user) {
		if (!Server.hasChannel(channel)) {
			return false;
		}
		
		return Server.getChannel(channel).addUser(user);
	}
	
	/**
	 * Attempts to remove a user from a given channel<br />
	 * Note: this does not create a channel if it does not already exist
	 * @param channel the channel to leave
	 * @param user the user to remove
	 * @return true if the user was removed from the channel, false if the channel does not exist, is invalid, or the user is not in the channel
	 */
	public static boolean leaveChannel(String channel, String user) {
		if (!Server.hasChannel(channel)) {
			return false;
		}
		
		return Server.getChannel(channel).removeUser(user);
	}
	
	/**
	 * Attempts to remove the user from all channels
	 * @param user the user to remove
	 */
	public static void leaveAllChannels(String user) {
		for (ServerChannel chan : channels) {
			chan.removeUser(user);
		}
	}
	
	/**
	 * Attempts to create a channel with a given name and auto-join the user
	 * @param channel the name of the channel
	 * @param creator the user creating the channel
	 * @return true if the channel was created, false if the channel already exists or is an invalid format
	 */
	public static boolean createChannel(String channel, String creator) {
		if (!channel.matches(Server.CHANNEL_REGEX)) {
			// Channel has invalid format
			return false;
		}
		if (Server.hasChannel(channel)) {
			return false;
		}
		return channels.add(new ServerChannel(channel, creator, false));
	}
	
	/**
	 * Checks for the existence of a channel with a given name
	 * @param channel the name of the channel
	 * @return true if the channel exists, false if the channel does not exists or is an invalid format
	 */
	public static boolean hasChannel(String channel) {
		if (!channel.matches(Server.CHANNEL_REGEX)) {
			// Channel has invalid format.
			// I check the channel format because if an invalid channel is given, e.g. "#mychan #mychan2",
			// it would give inaccurate results
			return false;
		}
		return Server.getChannels(false).contains(channel + " ") || Server.getChannels(false).contains(channel + "]");
	}
	
	/**
	 * Finds a channel with the given name
	 * @param channelName the channel to search for
	 * @return the ServerChannel object, or null if no channel exists with that name
	 */
	private static ServerChannel getChannel(String channelName) {
		for (ServerChannel sc : channels) {
			if (sc.getName().equals(channelName)) {
				return sc;
			}
		}
		return null;
	}
	
	/**
	 * Retrieves all the ServerThreads for users in a given channel
	 * @param channelName the name of the channel
	 * @return all the ServerThread objects in the channel, or null if the channel does not exist
	 */
	private static ArrayList<ServerThread> getChannelUsers(String channelName) {
		ServerChannel sc = Server.getChannel(channelName);
		if (sc == null) {
			return null;
		}
		
		ArrayList<ServerThread> clients = (ArrayList<ServerThread>) activeClients.stream()
											.filter(client -> sc.hasUser(client.getUserName()))
											.collect(Collectors.toList());
		/*String clientNames = String.join(", ", clients.stream()
											.map(ServerThread::getUserName)
											.collect(Collectors.toList()));
		System.out.println("[Debug] Users in channel " + channelName + ": " + clientNames);*/
		return clients;
	}
	
	/**
	 * Gets all users connected to the server in a channel in the user-readable format [user1, user_2, user353, admin]
	 * or space-separated format [user1 user_2 user353 admin]
	 * @return the list of connected users
	 */
	public static String getConnectedUsers(boolean userReadableFormat, String channel) {
		ServerChannel chan = null;
		for (ServerChannel sc : channels) {
			if (sc.getName().equalsIgnoreCase(channel)) {
				chan = sc;
				break;
			}
		}
		if (chan == null) {
			return "";
		}
		ArrayList<String> chanUsers = chan.getUsers();
		
		StringBuilder users = new StringBuilder();
		if (userReadableFormat) {
			// Format: "user1, user2, user3"
			users.append(String.join(", ", chanUsers));
		} else {
			// Format: "[user1 user2 user3]"
			users.append("[");
			users.append(String.join(" ", chanUsers));
			users.append("]");
		}
		
		return users.toString();
	}
	
	/**
	 * Gets all channels of the server in the user-readable format [#chan1, #chan2, #main, #secretclub]
	 * or space-separated format [#chan1 #chan2 #main #secretclub]
	 * @param userReadableFormat if the format will be for display purposes or client parsing
	 * @return the list of connected users
	 */
	public static String getChannels(boolean userReadableFormat) {
		StringBuilder channelList = new StringBuilder();
		if (userReadableFormat) {
			// Format: "user1, user2, user3"
			channelList.append(String.join(", ", channels.stream()
											.map(ServerChannel::getName)
											.sorted((channel1, channel2) -> channel1.compareTo(channel2))
											.collect(Collectors.toList())
											));
		} else {
			// Format: "[user1 user2 user3]"
			channelList.append("[");
			channelList.append(String.join(" ", channels.stream()
											.map(ServerChannel::getName)
											.sorted((channel1, channel2) -> channel1.compareTo(channel2))
											.collect(Collectors.toList())
											));
			channelList.append("]");
		}
		
		return channelList.toString();
	}
	
	/**
	 * Updates the name of a user in all channels they are connected to
	 * @param oldName the former name of the user
	 * @param newName the new name of the user
	 * @return true if the user was updated in all channels they were active in, false if one or more channels failed to update
	 */
	public static boolean updateName(String oldName, String newName) {
		ArrayList<ServerChannel> userConnectedChannels = (ArrayList<ServerChannel>) channels.stream()
															.filter(c -> c.hasUser(oldName))
															.collect(Collectors.toList());
		boolean allUpdated = true;
		for (ServerChannel sc : userConnectedChannels) {
			if (!sc.replaceUser(oldName, newName))
				allUpdated = false;
		}
		return allUpdated;
	}
	
	/**
	 * Gets the channels a user has joined on the server in the user-readable format
	 * [#chan1, #chan2, #main, #secretclub] or space-separated format [#chan1 #chan2 #main #secretclub]
	 * @param userReadableFormat if the format will be for display purposes or client parsing
	 * @param user the user to get the connected channels for
	 * @return the list of connected users
	 */
	public static String getChannels(boolean userReadableFormat, String user) {
		StringBuilder channelList = new StringBuilder();
		if (userReadableFormat) {
			// Format: "user1, user2, user3"
			channelList.append(String.join(", ", channels.stream()
											.filter(c -> c.hasUser(user))
											.map(ServerChannel::getName)
											.sorted((channel1, channel2) -> channel1.compareTo(channel2))
											.collect(Collectors.toList())
											));
		} else {
			// Format: "[user1 user2 user3]"
			channelList.append("[");
			channelList.append(String.join(" ", channels.stream()
											.filter(c -> c.hasUser(user))
											.map(ServerChannel::getName)
											.sorted((channel1, channel2) -> channel1.compareTo(channel2))
											.collect(Collectors.toList())
											));
			channelList.append("]");
			//System.out.println("[Debug]: " + user + " in channels: " + channelList.toString());
		}
		
		return channelList.toString();
	}
	
	@Override
	public void run() {
		// Every 10 seconds:
		// Delete any channels that can be deleted
		while(true) {
			System.out.println("Checking for inactive channels...");
			int totalChannels = Server.channels.size();
			ArrayList<String> removed = new ArrayList<String>();
			ArrayList<String> unremoveable = new ArrayList<String>();
			//for (ServerChannel sc : Server.channels) {
			for (int i = 0; i < Server.channels.size(); i++) {
				ServerChannel sc = Server.channels.get(i);
				if (sc.canClose()) {
					if (Server.channels.remove(sc)) {
						i--; // Prevent skipping over the next channel after removing the current channel
							// e.g. I remove channel 5. Channel 6 is now at index 6, but i=6 will point
							// to channel 7. Decrementing ensures I can check channel 6.
						removed.add(sc.getName());
					} else {
						unremoveable.add(sc.getName());
					}
				}
			}
			System.out.println("Of " + totalChannels + " channel(s), " + (removed.size() + unremoveable.size()) + " could be removed."
								+ (removed.size() > 0 ? "\nSuccessfully removed " + removed.size() + " channel(s): " + String.join(", ", removed) : "")
								+ (unremoveable.size() > 0 ? "\nUnable to remove " + unremoveable.size() + " channels: " + String.join(", ", unremoveable) : ""));
			try {
				Thread.sleep(30000); // Update every 30 seconds
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Starts the server process
	 * @param args a String array with potential arguments "-p" (port), and "-c" (max clients allowed)
	 */
	public static void main(String[] args) {
		// Parse args
		if (args.length > 0) {
			ArrayList<String> argList = new ArrayList<String>(Arrays.asList(args));
			String helpMessage = "ITSC 3166 Java chat server usage: java Client [-h] [-c max clients] [-p port]\n" +
								"\t-h: this help menu\n" +
								"\t-c [max clients]: the maximum number of clients that can be connected at a time (default: 10)\n" +
								"\t-p [port]: port number to listen on (default: 5000)";
			// Parse parameters
			if (argList.contains("-h")) {
				Client.displayMessage(helpMessage);
			}
			if (argList.contains("-p")) {
				int pos = argList.indexOf("-p");
				if (args.length >= pos + 1) {
					try {
						port = Integer.parseInt(args[pos + 1]);
						if (port < 1 || port > 65535) { // Valid port range
							Client.displayMessage("Error: -p given but invalid port was supplied (must be a whole number from 1 to 65535.");
							Client.displayMessage(helpMessage);
							System.exit(1);
						}
					} catch (NumberFormatException e) { // Non-int value was supplied
						Client.displayMessage("Error: -p given but invalid port was supplied (must be a whole number from 1 to 65535.");
						Client.displayMessage(helpMessage);
						System.exit(1);
					}
				} else {
					Client.displayMessage("Error: -p given but no port was supplied.");
					Client.displayMessage(helpMessage);
					System.exit(1);
				}
			}
			if (argList.contains("-c")) {
				int pos = argList.indexOf("-c");
				if (args.length >= pos + 1) {
					try {
						maxClients = Integer.parseInt(args[pos + 1]);
						if (maxClients < 1 || maxClients > 128) { // Valid port range
							Client.displayMessage("Error: -c given but max clients was not supplied (must be a whole number from 1 to 128.");
							Client.displayMessage(helpMessage);
							System.exit(1);
						}
					} catch (NumberFormatException e) { // Non-int value was supplied
						Client.displayMessage("Error: -c given but max clients was not supplied (must be a whole number from 1 to 128.");
						Client.displayMessage(helpMessage);
						System.exit(1);
					}
				} else {
					Client.displayMessage("Error: -c given but max clients was not supplied.");
					Client.displayMessage(helpMessage);
					System.exit(1);
				}
			}
		}
		
		ServerSocket server = null;
		Socket socket = null;
		String[] otherCommands = {"/help", "/nick", "/ping", "/users"};
		String[] quitAliases = {"/close", "/disconnect", "/exit", "/quit"};
		String[] whisperAliases = {"/w", "/whisper"};
		String[] invalidNames = {"client", "host", "server", "sys", "system"};
		reservedNames.addAll(Arrays.asList(invalidNames));
		quitCommands.addAll(Arrays.asList(quitAliases));
		whisperCommands.addAll(Arrays.asList(whisperAliases));
		commands.addAll(Arrays.asList(otherCommands));
		commands.addAll(quitCommands);
		commands.addAll(whisperCommands);
		channels.add(new ServerChannel(Server.DEFAULT_CHANNEL, true)); // Add default channel
		
		try {
			server = new ServerSocket(port);
			new Server().start();
			System.out.println("Server started, waiting for clients");
			
			while(true) {
				if (activeClients.size() <= maxClients) {
					socket = server.accept();
					totalClients++; // this is a running total of all the clients who have ever connected (for sequential numbering of default usernames)
					int clients = activeClients.size() + 1;
					System.out.println("Client " + clients + " connected");
					activeClients.add(new ServerThread(socket, totalClients));
					activeClients.get(clients - 1).start(); // start thread for client
					broadcast(activeClients.get(clients - 1).getUserName() + " has joined the chat", SYSTEM_NAME);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
